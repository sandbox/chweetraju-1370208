<?php

/**
 * @file
 * Administrative page callbacks for the avatar_selection module.
 */

/**
 * Select which form will be shown to the user, according to the permissions.
 *
 * @param $op
 *   Default NULL; the action the user wants to do after the function checks
  &   the permission.
 * @return
 *   Return the structure of the form.
 */
function user_avatar_selection_settings_page($op = NULL) {

  switch ($op) {
    case 'edit':
      $output = drupal_get_form('user_avatar_selection_config_form');
      break;
    case 'upload':
      $output = drupal_get_form('user_avatar_selection_upload_form');
      break;
    case 'list':
      $output = drupal_get_form('user_avatar_selection_edit_form');
      break;
    default:
      $form[] = array(
        '#type' => 'fieldset',
        '#title' => t('Add another'),
      );
      $output = drupal_get_form('user_avatar_selection_config_form');
      break;
  }
  return $output;
}

// <editor-fold defaultstate="collapsed" desc="Config Form">

/**
 * Create the form structure for configuring the avatar module settings; seen
 * in the 'Configure' tab under the Avatar Selection administration page.
 *
 * @return
 *   Return the structure of the form.
 */
function user_avatar_selection_config_form($form) {
  if (!variable_get('user_pictures', 0)) {
    drupal_set_message(t('User Pictures option is disabled.  '
        . 'You will need to enable this option before you can use the Avatar Selection module.  '
        . 'You may configure this setting on the <a href="@url">User settings</a> page.'
        , array('@url' => url('admin/user/settings'))));
  }

  // To store how many avatars per page are displayed.
  $form['update']['avatar_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('How many avatars per page'),
    '#description' => t('The number of avatars to show per page.'),
    '#default_value' => variable_get('user_avatar_selection_avatar_per_page', 30),
    '#size' => 3,
  );
  $form['update']['force_set_image_reg'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force users to select an avatar image on user registration.'),
    '#description' => t('This only applies on the user registration screen.'),
    '#default_value' => variable_get('user_avatar_selection_force_user_avatar_reg', FALSE),
  );
  $form['update']['force_set_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force users to select an avatar image when editing their account'),
    '#description' => t('This only applies when the user is editing their account details and image uploads are disabled.'),
    '#default_value' => variable_get('user_avatar_selection_force_user_avatar', FALSE),
  );
  $form['update']['set_random_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable random default avatar image.'),
    '#description' => t("Automatically set a random avatar image to be used when the user doesn't set one for their account."),
    '#default_value' => variable_get('user_avatar_selection_set_random_default', FALSE),
  );
  $form['update']['distinctive_avatars'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable unique avatars.'),
    '#description' => t("Only allow users to pick an avatar that isn't already in use by another user.  If there are no available avatars left, the default avatar image will be used."),
    '#default_value' => variable_get('user_avatar_selection_distinctive_avatars', FALSE),
  );

  if (module_exists('imagecache')) {
    // Load imagecache presets
    $presets = array();
    $presets[] = '';
    foreach (imagecache_presets() as $preset) {
      $presets[$preset['presetname']] = check_plain($preset['presetname']);
    }

    $form['update']['imagecache_preset'] = array(
      '#type' => 'select',
      '#title' => t('Imagecache preset'),
      '#default_value' => variable_get('user_avatar_selection_imagecache_preset', FALSE),
      '#options' => $presets,
      '#description' => t('Choose an imagecache preset to format the avatars in the selection list with.  The <a href="@url">ImageCache Profiles</a> module can be used to format the avatars in other locations.', array('@url' => url('http://drupal.org/project/imagecache_profiles'))),
    );
  }

  $form['update']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Validate the submission.
 *
 * Ensure the number of avatars page setting is numeric.
 *
 * @param $form
 *   General variable used in drupal, defining the structure & the fields of a
 *   form.
 * @param &$form_state
 *   General reference, used to control the processing of the form.
 */
function user_avatar_selection_config_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] != t('Update')) return;

  if (!is_numeric($form_state['values']['avatar_per_page'])) {
    form_set_error('avatar_per_page', t('Must be a number.'));
  }
}

/**
 * Submit the settings form in the Avatar Selection administration page.
 *
 * @param $form
 *   General variable used in drupal, defining the structure & the fields of a
 *   form.
 * @param &$form_state
 *   General reference, used to control the processing of the form.
 */
function user_avatar_selection_config_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  if ($op != t('Update')) return;

  // Save system variables.
  variable_set('user_avatar_selection_force_user_avatar_reg', $form_state['values']['force_set_image_reg']);
  variable_set('user_avatar_selection_force_user_avatar', $form_state['values']['force_set_image']);
  variable_set('user_avatar_selection_avatar_per_page', $form_state['values']['avatar_per_page']);
  variable_set('user_avatar_selection_set_random_default', $form_state['values']['set_random_default']);
  variable_set('user_avatar_selection_distinctive_avatars', $form_state['values']['distinctive_avatars']);
  if (module_exists('imagecache')) variable_set('user_avatar_selection_imagecache_preset', $form_state['values']['imagecache_preset']);

  drupal_set_message(t('Configuration has been updated.'));
}

// </editor-fold>
//
// <editor-fold defaultstate="collapsed" desc="Upload Form">

/**
 * Create the form structure for uploading an avatar.
 *
 * @return
 *   Return the structure of the form.
 */
function user_avatar_selection_upload_form($form) {
  if (!variable_get('user_pictures', 0)) {
    drupal_set_message(t('User Pictures option is disabled.  '
        . 'You will need to enable this option before you can use the Avatar Selection module.  '
        . 'You may configure this setting on the <a href="@url">User settings</a> page.'
        , array('@url' => url('admin/user/settings'))));
  }

  $form['#attributes']['enctype'] = 'multipart/form-data';

  $dir = file_build_uri('user_avatar_selection');
  file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
  $form['bulk_upload'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk Upload / Delete'),
    '#description' => t("To upload a large number of avatars, first copy the images manually to the %dir folder, using ftp for example.  To make these new avatar images available, check the 'Scan for new avatars' option. All new images will then be added to the list.  By removing files from this directory and checking the box, you can also perform a bulk delete.", array('%dir' => $dir)),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['bulk_upload']['scan_avatars'] = array(
    '#type' => 'checkbox',
    '#title' => t('Scan avatars'),
    '#description' => t('All new avatar images found will be added to the list of available avatars with the name, weight and permissions defined below.  Scanning for new avatars may be slow depending on the number of files.  All avatar entries which no longer have a corresponding file will be be removed.'),
    '#default_value' => 0,
  );

  $form['single_upload'] = array(
    '#type' => 'fieldset',
    '#title' => t('Single Image Upload'),
    '#description' => t("To upload a single avatar image."),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );
  $form['single_upload']['picture_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload image'),
    '#size' => 48,
    '#description' => t('Maximum dimensions are %dimensions and the maximum size is %size kB.  Images must have one of the following extensions (case sensitive): png, jpg, jpeg, gif, PNG, JPG, JPEG, GIF.', array('%dimensions' => variable_get('user_picture_dimensions', '85x85'), '%size' => variable_get('user_picture_file_size', 30))) . ' ' . variable_get('user_picture_guidelines', ''),
  );

  $form['single_upload']['avatar_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t("Image name or title which will be displayed when hovering over the image.  It's also used in conjunction with the weight setting for sorting the avatars."),
    '#size' => 48,
  );
  $form['single_upload']['avatar_weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 100,
    '#description' => t('Avatars with a lower weight appear before higher weighted avatars in lists.'),
  );
  $form['single_upload']['permissions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Permissions'),
    '#weight' => 2,
  );
  $form['single_upload']['permissions']['access'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User Roles'),
    '#options' => _user_avatar_selection_get_user_roles(),
    '#description' => t('Only the checked roles will be able to see this avatar icon; if no roles are checked, access will not be restricted.'),
  );

  if (module_exists('og')) {
    $form['single_upload']['permissions']['og_access'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Organic Groups'),
      '#options' => og_all_groups_options(),
      '#description' => t('Only users in the checked organic groups will be able to see this avatar icon; if no groups are checked, access will not be restricted.'),
    );
  }

  $form['upload'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
    '#weight' => 10,
  );

  return $form;
}

/**
 * Submit the image upload form in the Avatar Selection administration page.
 *
 * @param $form
 *   General variable used in drupal, defining the structure & the fields of a
 *   form.
 * @param &$form_state
 *   General reference, used to control the processing of the form.
 */
function user_avatar_selection_upload_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  if ($op != t('Upload')) return;

  //
  // Mass image upload/delete
  if ($form_state['values']['scan_avatars'] == 1) {
    $count = _user_avatar_selection_scan_images();
    drupal_set_message(t('Scan complete: %added new avatars found. %deleted avatars removed.', array('%added' => $count['add'], '%deleted' => $count['delete'])));
  }

  //
  // Single image upload
  $dir = file_build_uri('user_avatar_selection');
  if (!file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
    form_set_error('picture_upload', t('Directory not writable: !dir', array('!dir' => $dir)));
    return;
  }
  $file = file_save_upload('picture_upload', array(), $dir, FILE_EXISTS_RENAME);
  if (!$file) return;

  //
  // Get file details.
  $access = array_keys(array_filter($form_state['values']['access']));
  $name = $form_state['values']['avatar_name'];
  $weight = $form_state['values']['avatar_weight'];
  $og = array();
  if (module_exists('og')) $og = array_keys(array_filter($form_state['values']['og_access']));

  //
  // Add image
  _user_avatar_selection_add_avatar($file, $name, $weight, $access, $og);
  drupal_set_message(t('New image saved.'));
}

/**
 * Scan the directory for new files.
 *
 * @param $name
 *   The name of the avatar(s).
 * @param $access
 *   The permission - the value determines which user roles have rights to see
 *   the avatar.
 * @param $og
 *   Organic group (optional).
 * @param $weight
 *   Avatar weight to assign.
 * @return
 *   Number of images found.
 */
function _user_avatar_selection_scan_images() {
  $add_count = 0;
  $delete_count = 0;

  // Scan directory for avatars
  $dir = file_build_uri('user_avatar_selection');
  file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
  $mask = '/.*\.(gif|GIF|Gif|jpg|JPG|Jpg|jpeg|JPEG|Jpeg|png|PNG|Png)/';
  $listings = file_scan_directory($dir, $mask);

  // Get existing avatars from db
  $avatars = array();
  $result = db_query("SELECT avatar FROM {user_avatar_selection} avs");
  foreach ($result as $avatar) $avatars[$avatar->avatar] = $avatar->avatar;

  // Add new avatars
  foreach ($listings as $listing) {
    // Remove matching records from avatars array.
    if (in_array($listing->filename, $avatars)) {
      unset($avatars[$listing->filename]);
      continue;
    }
    _user_avatar_selection_add_avatar($listing, $listing->name);
    $add_count++;
  }

  // Remove records from database where we have an avatar entry but no corresponding file.
  foreach ($avatars as $avatar) {
    _user_avatar_selection_delete_avatar($avatar);
    $delete_count++;
  }

  $count['add'] = $add_count;
  $count['delete'] = $delete_count;
  return $count;
}

// </editor-fold>
//
///**
// * Validate the submission.
// *
// * Check if Delete has been chosen AND a checkbox has been selected.
// *
// * @param $form
// *   General variable used in drupal, defining the structure & the fields of a
// *   form.
// * @param &$form_state
// *   General reference, used to control the processing of the form.
// */
//function user_avatar_selection_delete_form_validate($form, &$form_state) {
//  if ($form_state['values']['op'] != t('Delete')) return;
//
//  if (count(array_filter($form_state['values']['images'])) == 0) {
//    form_set_error('images', t('Please select images to delete.'));
//  }
//}
// <editor-fold defaultstate="collapsed" desc="Manage Avatars Form">

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function user_avatar_selection_roles_page($op = NULL) {
  $output = '';

  // Display the form where appropriate.
  if (isset($op) && ($op == 'role' || $op == 'og')) {
    $output = array();
    $output['user_avatar_selection_edit_form'] = drupal_get_form('user_avatar_selection_edit_form');
    return $output;
  }

  // Display the number of avatars per role / group.
  $avs_access = array();
  $og_access = array();
  $avs_access[0] = 0;
  $og_access[0] = 0;

  // Get the list of access roles and initialise the count to 0.
  $roles = _user_avatar_selection_get_user_roles();
  foreach ($roles as $rid => $role_name) $avs_access[$rid] = 0;

  // Get the list of organic groups and initialise the count to 0.
  if (module_exists('og')) {
    $ogroups = og_all_groups_options();
    foreach ($ogroups as $ogid => $ogroup_name) $og_access[$ogid] = 0;
  }

  // Get the total number of avatars available on the system.
  $total_count = 0;
  $result = db_query("SELECT count(*) AS count FROM {user_avatar_selection} avs");
  foreach ($result as $avatar) $total_count = $avatar->count;
  $output .= '<p>' . t('There is a total of %count avatars configured.', array('%count' => $total_count)) . '</p>';

  // Get the count of avatars per role.
  $result = db_query("SELECT avsr.rid, count(*) AS count FROM {user_avatar_selection} avs LEFT JOIN {user_avatar_selection_roles} avsr ON avs.aid = avsr.aid GROUP BY avsr.rid");
  foreach ($result as $avatar) {
    if (empty($avatar->rid)) $avs_access[0] += $avatar->count;
    else $avs_access[$avatar->rid] += $avatar->count;
  }

  // Get the count of avatars per organic group.
  $result = db_query("SELECT avso.ogid, count(*) AS count FROM {user_avatar_selection} avs LEFT JOIN {user_avatar_selection_og} avso ON avs.aid = avso.aid GROUP BY avso.ogid");
  foreach ($result as $avatar) {
    if (empty($avatar->ogid)) $og_access[0] += $avatar->count;
    else $og_access[$avatar->ogid] += $avatar->count;
  }

  // Format the user roles table.
  $avs_rows = array();
  $header = array(t('User Role'), t('Number of Avatars'), t('Operations'));
  $edit = l(t('edit'), 'admin/config/people/user_avatar_selection/edit/role/0');
  $avs_rows[] = array(t('Available to all roles'), $avs_access[0], $edit);
  foreach ($roles as $rid => $role_name) {
    $edit = l(t('edit'), 'admin/config/people/user_avatar_selection/edit/role/' . $rid);
    $avs_rows[] = array($role_name, $avs_access[$rid], $edit);
  }
  $output .= theme('table', array('header' => $header, 'rows' => $avs_rows));

  // Format the organic groups table.
  if (module_exists('og')) {
    $og_rows = array();
    $header = array(t('Organic Group'), t('Number of Avatars'));
    $edit = l(t('edit'), 'admin/config/people/user_avatar_selection/edit/og/0');
    $og_rows[] = array(t('Available to all groups'), $og_access[0], $edit);
    foreach ($ogroups as $ogid => $ogroup_name) {
      $edit = l(t('edit'), 'admin/config/people/user_avatar_selection/edit/og/' . $ogid);
      $og_rows[] = array($ogroup_name, $og_access[$ogid], $edit);
    }
    $output .= theme('table', array('header' => $header, 'rows' => $og_rows));
  }
  return $output;
}

/**
 * Create the form structure for listing the avatars and managing them in the
 * 'Manage Avatars' tab under the Avatar Selection administration page.
 *
 * @param $form_state
 *   General variable, used to control the processing of the form.
 * @return
 *   Return the structure of the form.
 */
function user_avatar_selection_edit_form($form, $form_state) {
  $form = array();

  if (!variable_get('user_pictures', 0)) {
    drupal_set_message(t('User Pictures option is disabled.  '
        . 'You will need to enable this option before you can use the Avatar Selection module.  '
        . 'You may configure this setting on the <a href="@url">User settings</a> page.'
        , array('@url' => url('admin/user/settings'))));
  }

  drupal_add_css(drupal_get_path('module', 'user_avatar_selection') . '/css/user_avatar_selection.css');

  $set_type = arg(5);
  $set_id = arg(6);
  $avatars = _user_avatar_selection_get_themed_avatars('', $set_type, $set_id);
  if ($avatars->total < 1) {
    drupal_set_message(t('There are no avatars configured.'));
    return $form;
  }

  $step = (!isset($form_state['values'])) ? 'list' : 'edit';
  $form['step'] = array(
    '#type' => 'value',
    '#value' => $step,
  );

  if ($step == 'list') {

    if ($set_type == 'role') {
      $sets = _user_avatar_selection_get_user_roles();
    }
    elseif ($set_type == 'og') {
      $sets = og_all_groups_options();
    }

    $sets[0] = t('All roles');
    drupal_set_title(t('Manage Avatars - %name', array('%name' => $sets[$set_id])), PASS_THROUGH);
    drupal_add_js(drupal_get_path('module', 'user_avatar_selection') . '/js/user_avatar_selection_pager.js');
    drupal_add_js(drupal_get_path('module', 'user_avatar_selection') . '/js/user_avatar_selection.js');

    $form['select_avatar'] = array(
      '#type' => 'radios',
      '#title' => t('Select an avatar to edit'),
      '#options' => $avatars->themed_items,
      '#required' => TRUE,
      '#attributes' => array('class' => array('user_avatar_select')),
    );

    $form['search'] = array(
      '#type' => 'submit',
      '#value' => t('Edit'),
      '#submit' => array('user_avatar_selection_edit_list_form_submit'),
    );
  }
  elseif ($step == 'edit') {
    drupal_set_title(t('Manage Avatars'));
    $form_state['#redirect'] = array('admin/config/people/user_avatar_selection/edit');
    $roles = _user_avatar_selection_get_user_roles();
    $aid = 0;
    $avs_access = $og_access = array();
    $weight = 0;
    $name = '';

    $result = db_query("SELECT avs.aid, avatar, name, weight, rid, ogid FROM {user_avatar_selection} avs LEFT JOIN {user_avatar_selection_roles} avsr ON avs.aid = avsr.aid LEFT JOIN {user_avatar_selection_og} avso ON avs.aid = avso.aid WHERE avatar = :avatar ORDER BY weight, name, avatar", array(':avatar' => $form_state['values']['select_avatar']));
    foreach ($result as $avatar) {
      $aid = $avatar->aid;
      $name = $avatar->name;
      $weight = $avatar->weight;
      if ($avatar->rid) array_push($avs_access, $avatar->rid);
      if ($avatar->ogid) array_push($og_access, $avatar->ogid);
    }

    $image_path = file_build_uri('user_avatar_selection');
    file_prepare_directory($image_path, FILE_CREATE_DIRECTORY);
    $selected_avatar = $form_state['values']['select_avatar'];
    $image = theme('image', array('path' => $image_path . '/' . $selected_avatar));
    $form['avatar_image'] = array('#value' => $image);
    $form['aid'] = array(
      '#type' => 'value',
      '#value' => $aid,
    );

    $form['select_avatar'] = array(
      '#type' => 'value',
      '#value' => $form_state['values']['select_avatar'],
    );

    $form['avatar_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t("Image name or title which will be displayed when hovering over the image.  It's also used in conjunction with the weight setting for sorting the avatars."),
      '#size' => 48,
      '#default_value' => $name,
    );
    $form['avatar_weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#delta' => 100,
      '#description' => t('Avatars with a lower weight appear before higher weighted avatars in lists.'),
      '#default_value' => $weight,
    );
    $form['permissions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Permissions'),
      '#weight' => 1,
    );
    $form['permissions']['access'] = array(
      '#type' => 'checkboxes',
      '#title' => t('User Roles'),
      '#default_value' => $avs_access,
      '#options' => $roles,
      '#description' => t('Only the checked roles will be able to see this avatar icon; if no roles are checked, access will not be restricted.'),
    );

    if (module_exists('og')) {
      $form['permissions']['og_access'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Organic Groups'),
        '#default_value' => $og_access,
        '#options' => og_all_groups_options(),
        '#description' => t('Only users in the checked organic groups will be able to see this avatar icon; if no groups are checked, access will not be restricted.'),
      );
    }

    $form['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#weight' => 9,
      '#submit' => array('user_avatar_selection_edit_update_form_submit'),
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 10,
      '#submit' => array('user_avatar_selection_edit_delete_form_submit'),
    );
  }
  return $form;
}

/**
 * Submit the image list form in the Avatar Selection - 'Manage Avatars' page.
 *
 * Function called when the Edit button is pressed.
 * Practically it sets the $form_state['rebuild'] value to true, which will
 * determine the rebuilding of the page.
 *
 * @param $form
 *   General variable used in drupal, defining the structure & the fields of a
 *   form.
 * @param &$form_state
 *   General reference, used to control the processing of the form.
 */
function user_avatar_selection_edit_list_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit the image list form in the Avatar Selection - 'Manage Avatars' page.
 *
 * Function called when the Update button is pressed.
 *
 * @param $form
 *   General variable used in drupal, defining the structure & the fields of a
 *   form.
 * @param &$form_state
 *   General reference, used to control the processing of the form.
 */
function user_avatar_selection_edit_update_form_submit($form, &$form_state) {
  $og = array();
  $access = array_keys(array_filter($form_state['values']['access']));
  if (module_exists('og')) $og = array_keys(array_filter($form_state['values']['og_access']));

  $filename = $form_state['values']['select_avatar'];
  $name = $form_state['values']['avatar_name'];
  $weight = $form_state['values']['avatar_weight'];
  $aid = $form_state['values']['aid'];
  _user_avatar_selection_update_avatar($aid, $filename, $name, $weight, $access, $og);
  drupal_set_message(t('Image updated.'));
}

/**
 * Submit the image list form in the Avatar Selection - 'Manage Avatars' page.
 *
 * Function called when the Delete button is pressed, and deletes the selected
 * avatar(s).
 *
 * @param $form
 *   General variable used in drupal, defining the structure & the fields of a
 *   form.
 * @param &$form_state
 *   General reference, used to control the processing of the form.
 */
function user_avatar_selection_edit_delete_form_submit($form, &$form_state) {
  $image = $form_state['values']['select_avatar'];
  $deleted = _user_avatar_selection_delete_avatar($image);
  if ($deleted) drupal_set_message(t('Image deleted.'));
}

// </editor-fold>

/**
 * Return a list of the existing roles.
 *
 * @return
 *   Return the role id(s).
 */
function _user_avatar_selection_get_user_roles() {
  $result = db_query("SELECT r.rid, r.name FROM {role} r ORDER BY r.name");

  $rids = array();
  foreach ($result as $obj) $rids[$obj->rid] = $obj->name;
  return $rids;
}

/**
 * @param $file
 *   The file object.
 * @param $name
 *   The avatar name, used as the image alternative text on the forms.
 * @param $weight
 *   The weight of the avatar.  Lower weighted avatars appear before higher
 *   weighted avatars in the list (optional).
 * @param $access
 *   Array of roles - the value determines which user roles have rights to see
 *   the avatar (optional).
 * @param $og
 *   Array of organic groups (optional).
 */
function _user_avatar_selection_add_avatar($file, $name, $weight = 0, $access = array(), $og = array()) {
  //
  // Check if it is an image file
  $image_info = image_get_info($file->uri);
  if (!$image_info) {
    file_delete($file);
    drupal_set_message(t("File ':filename' does not appear to be a valid image file."
        , array(':filename' => $file->filename)));
    return;
  }

  //
  // Update file details
  global $user;
  $file->uid = $user->uid;
  $file->status = FILE_STATUS_PERMANENT;
  $file->filemime = $image_info['mime_type'];
  $file = file_save($file);
  if (!$file) {
    drupal_set_message(t("Failed to save the avatar ':filename'. Could not update the file details."
        , array(':filename' => $file->filename)), 'error');
    return;
  }

  //
  // Update file usage table
  file_usage_add($file, 'user_avatar_selection', 'user', $user->uid);

  //
  // Add avatar
  $aid = db_insert('user_avatar_selection')
    ->fields(array(
      'fid' => $file->fid,
      'avatar' => $file->filename,
      'name' => $name,
      'weight' => $weight,
    ))
    ->execute();
  if (!$aid) {
    drupal_set_message(t("Failed to save the avatar ':filename'."
        , array(':filename' => $file->filename)), 'error');
    return;
  }

  //
  // Setup Permissions
  _user_avatar_selection_set_avatar_access($aid, $access, $og);
}

/**
 * @param $aid
 *   The avatar identifier.
 * @param $filename
 *   The avatar filename.
 * @param $name
 *   The avatar name, used as the image alternative text on the forms.
 * @param $weight
 *   The weight of the avatar.  Lower weighted avatars appear before higher
 *   weighted avatars in the list (optional).
 * @param $access
 *   Array of roles - the value determines which user roles have rights to see
 *   the avatar (optional).
 * @param $og
 *   Array of organic groups (optional).
 */
function _user_avatar_selection_update_avatar($aid, $filename, $name, $weight = 0, $access = array(), $og = array()) {
  if (!$aid) return;

  $result = db_update('user_avatar_selection')
    ->fields(array(
      'name' => $name,
      'weight' => $weight,
    ))
    ->condition('aid', $aid)
    ->condition('avatar', $filename)
    ->execute();

  $result = db_delete('user_avatar_selection_roles')
    ->condition('aid', $aid)
    ->execute();

  $result = db_delete('user_avatar_selection_og')
    ->condition('aid', $aid)
    ->execute();

  _user_avatar_selection_set_avatar_access($aid, $access, $og);
}

/**
 * @param $aid
 *   The avatar identifier.
 * @param $access
 *   Array of roles - the value determines which user roles have rights to see
 *   the avatar.
 * @param $og
 *   Array of organic groups.
 */
function _user_avatar_selection_set_avatar_access($aid, $access, $og) {
  if (!$aid) return;

  if (is_array($access) && count($access)) {
    foreach ($access as $rid) {
      $id = db_insert('user_avatar_selection_roles')
        ->fields(array(
          'aid' => $aid,
          'rid' => $rid,
        ))
        ->execute();
    }
  }

  if (is_array($og) && count($og)) {
    foreach ($og as $ogid) {
      $id = db_insert('user_avatar_selection_og')
        ->fields(array(
          'aid' => $aid,
          'ogid' => $ogid,
        ))
        ->execute();
    }
  }
}

/**
 * Delete the specified avatar image.
 *
 * @param $filename
 *   Path to the image to be deleted.
 * @return
 *   1 if deleted, otherwise 0.
 */
function _user_avatar_selection_delete_avatar($filename) {
  //
  // Get file id's
  $result = db_query("SELECT aid, fid FROM {user_avatar_selection} WHERE avatar = :avatar"
    , array(':avatar' => $filename))->fetchObject();
  if (!$result) return 0;

  //
  // Delete from our tables
  db_delete('user_avatar_selection')->condition('aid', $result->aid)->execute();
  db_delete('user_avatar_selection_roles')->condition('aid', $result->aid)->execute();
  db_delete('user_avatar_selection_og')->condition('aid', $result->aid)->execute();

  //
  // Delete the physical file and Drupal managed entries
  $file = file_load($result->fid);
  if (!is_object($file)) return 0;

  file_usage_delete($file, 'user_avatar_selection');
  return file_delete($file, TRUE);
}
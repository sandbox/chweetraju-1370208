
/**
 * Fetches the new page and puts it inline.
 *
 * @param form_id   - The id of the form whose action should be updated.
 * @param id   - The element id whose contents will get replaced.
 * @param url - The URL for the new page.
 * @param page  - The page number to request.
 */
function fetchPage(form_id, id, url, page) {
  jQuery(".user-avatar-select").css({'opacity': 0.5});
  jQuery("#avatar-selection-loading").show();
  jQuery.get(
    url
    , {page: page}
    , function(data, textStatus, jqXHR) {
      var selects = jQuery(data).find(id);
      jQuery(id).html(selects);
      
      var pager = jQuery(data).find(".avatar-selection-pager-nav");
      jQuery(".avatar-selection-pager-nav").html(pager);
      
      Drupal.attachBehaviors(data);
      
      var action = url + "?page="+page;
      jQuery(form_id).attr("action", action);
      jQuery("#avatar-selection-loading").hide();
      jQuery(".user-avatar-select").css({'opacity': 1});
      return false;
    });
  return false;
}
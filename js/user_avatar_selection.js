(function ($) {
  Drupal.behaviors.avatarSelectionRadioHandler = {
    attach: function(context) {
      radio_button_handler();
      image_click_handler();
    }
  };
})(jQuery);

function radio_button_handler() {
  // handle radio buttons
  jQuery('div.user-avatar-select input.form-radio').hide();
  jQuery('div.user-avatar-select img').hover(
    function(){
      jQuery(this).addClass("avatar-hover");
    },
    function(){
      jQuery(this).removeClass("avatar-hover");
    }
    );
}

function image_click_handler() {
  jQuery('div.user-avatar-select img').click(function(){
    jQuery("div.user-avatar-select img.avatar-select").each(function(){
      jQuery(this).removeClass("avatar-select");
      jQuery(this).parent().children("input").attr("checked", "");
    });
    jQuery(this).addClass("avatar-select");
    jQuery(this).parent().children("input").attr("checked", "checked");
  });
}
